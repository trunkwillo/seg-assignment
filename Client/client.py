import socket

import threading
import random
import json
import itertools
import pyDH
import time
from OpenSSL import crypto
import pkcs11
from pkcs11 import Attribute, ObjectClass, Mechanism
import OpenSSL
from cryptography.hazmat.primitives.asymmetric import rsa, padding

from tabulate import tabulate

import sys

from sys import getsizeof

import base64
from Crypto.Cipher import AES
from Crypto import Random

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.fernet import Fernet
import datetime

import os
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


from Crypto.Hash import SHA256


class SymCipher:
    def __init__( self, key ):
        self.key = key
        #add X '\0' till size is AES block
        self.pad = lambda s: s + b"\0" * ((AES.block_size - len(s)) % AES.block_size)
        #remove '\0''s
        self.unpad = lambda s: s.rstrip(b"\0")

    def encrypt( self, data ):
        data = self.pad(data)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new( self.key, AES.MODE_CBC, iv )

        return base64.b64encode(iv+cipher.encrypt(data))

    def decrypt( self, enc_data ):
        enc_data = base64.b64decode(enc_data)
        iv=enc_data[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return self.unpad(cipher.decrypt( enc_data[AES.block_size:] ))

##CC
def signCCMsg(msg):
        lib = '/usr/local/lib/libpteidpkcs11.dylib'
        lib = pkcs11.lib(lib)
        token = lib.get_token()
        with token.open() as session:
            privateKeys = session.get_objects({Attribute.CLASS: ObjectClass.PRIVATE_KEY})
            privateKey = None
            for key in privateKeys:
                if 'CITIZEN AUTHENTICATION KEY' in str(key):
                    privateKey = key
                    break
            if privateKey:
                signature = privateKey.sign(msg.encode('utf-8'),mechanism = Mechanism.SHA1_RSA_PKCS)
                return signature
            return None


def getCCandCert():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print(dir_path)
    #lib = 'C:\Windows\System32\libpteidpkcs11.so'
    lib = "/usr/local/lib/libpteidpkcs11.dylib"
    lib = pkcs11.lib(lib)
    token = lib.get_token()
    with token.open() as session:
        for cert in session.get_objects({Attribute.CLASS: ObjectClass.CERTIFICATE,}):
            cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_ASN1,cert[Attribute.VALUE],)
            subject = cert.get_subject()
            #Convert to PEM format
            cert = OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM,cert)

            return (subject.serialNumber,cert)

def verify_certificate_chain(certificate):
    trusted_certs = []
    crl_list = []
    print(os.getcwd())
    for files in os.listdir(
            os.getcwd()+"/../Server/Certs"):
        if files == ".DS_Store":
            continue


        _cert = open(
            os.getcwd()+"/../Server/Certs/" + files,
            'rb').read()
        trusted_certs.append(_cert)

    for files in os.listdir(
            os.getcwd()+"/../Server/CRL"):
        if files == ".DS_Store":
            continue


        _crl = open(
            os.getcwd()+"/../Server/CRL/" + files,
            'rb').read()
        crl_list.append(_crl)

    # cc,certificate=getCCandCert()
    certificate = crypto.load_certificate(crypto.FILETYPE_PEM, certificate)
    store = crypto.X509Store()

    for _cert in trusted_certs:
        try:
            pemCert = crypto.load_certificate(crypto.FILETYPE_ASN1, _cert)
            store.add_cert(pemCert)

        except:
            try:
                pemCert = crypto.load_certificate(crypto.FILETYPE_PEM, _cert)
                store.add_cert(pemCert)

            except Exception as e:
                continue

    for _crl in crl_list:
        try:
            pcrl = crypto.load_crl(crypto.FILETYPE_ASN1, _crl)
            store.add_crl(pcrl)

        except:
            try:
                pcrl = crypto.load_crl(crypto.FILETYPE_PEM, _crl)
                store.add_crl(pcrl)

            except Exception as e:
                continue

    store_ctx = crypto.X509StoreContext(store, certificate)

    try:
        store_ctx.verify_certificate()
        return True


    except Exception as e:
        print(e)
        return False



#Certificate functions
def generate_key():
    key = rsa.generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )
    return key

def print_key(private_key):
    pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )


def generate_self_signed(key):
    subject = issuer = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"PT"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Aveiro"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Aveiro"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"UA"),
        x509.NameAttribute(NameOID.COMMON_NAME, u"cert.ua.pt"),
    ])
    cert = x509.CertificateBuilder().subject_name(
        subject
    ).issuer_name(
        issuer
    ).public_key(
        key.public_key()
    ).serial_number(
     x509.random_serial_number()
    ).not_valid_before(
        datetime.datetime.utcnow()
    ).not_valid_after(
        datetime.datetime.utcnow() + datetime.timedelta(days=1)
    ).add_extension(
        x509.SubjectAlternativeName([x509.DNSName(u"localhost")]),
        critical=False,
    ).sign(key, hashes.SHA256(), default_backend())

    return cert

def store_key(key, name, passphrase):
     with open("securityUtils/keys/"+name+"key.pem", "wb+") as f:
        f.write(key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.BestAvailableEncryption(str.encode(passphrase)),
        ))

def store_cert(cert, name):
     with open("securityUtils/certs/certificate.pem", "wb+") as f:
        f.write(cert.public_bytes(serialization.Encoding.PEM))



# Bit Commitment

def bitCommitment(hand):
    handStr=','.join(hand)
    random1= base64.b64encode(os.urandom(16)).decode('utf-8')
    random2= base64.b64encode(os.urandom(16)).decode('utf-8')
    h=SHA256.new()
    toHash=handStr+random1+random2
    h.update(toHash.encode())
    b=h.hexdigest()
    toPublish={}
    toPublish["b"]=b
    toPublish["R1"]=random1
    toKeep={}
    toKeep["R2"]=random2
    toKeep["C"]=handStr

    return [toKeep, toPublish]

#Bit Commitment Validation

def validateBitCommitment(kept, published):
    h=SHA256.new()
    toHash=kept["C"]+published["R1"]+kept["R2"]
    h.update(toHash.encode())
    b=h.hexdigest()
    return published["b"]==b

def generateKeyDH(sw):
    password_provided = sw  # This is input in the form of a string
    password = password_provided.encode()  # Convert to type bytes
    salt = b'ola'  # CHANGE THIS - recommend using a key from os.urandom(16), must be of type bytes
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
        backend=default_backend()
    )
    key = base64.urlsafe_b64encode(kdf.derive(password))  # Can only use kdf once
    f = Fernet(key)
    return f




'''
    Protocolo de comunicação:
    - Jogador (Nota: 0 é o coupier)
    - Tipo de mensagem:        
        - Baralhar:
        "BARALHO" - [S->C] Baralha o baralho e armazena a chave simétrica
        "DEVOLVER" - [C->S] Devolve o deck baralhado e cifrado
        
        - Distribuição:
        "DISTRIBUICAO" - [Cy->CC] Recebe uma mensagem de um cliente (no campo jogador define source/destiny) cifrada por diffie-hellman, chance de retirar carta ou não
        "DISTRIBUICAO" - [CC->Cx] Envia para um jogador random uma mensagem cifrada por Diffie Hellman
        "FIM_DISTRIBUICAO" - [C->S] Avisa que terminou a distribuição (chance aleatória de isto acontecer quando acabam as cartas válidas)
        "COMMIT_REQUEST" - [S->C] Requer o commit dos bits da mão inicial ainda cifrada
        "COMMIT_REPLY" - [C->S] Envia a sua mão commited ainda cifrada
        "SHUFFLE_KEY_REQUEST" - [S->C] Recebe as chaves simétricas para decifrar a mão, responde com ok caso sejam cartas válidas
        "SHUFFLE_KEY_REPLY" - [C->S] Envia a chave simétrica
        "SHUFFLE_KEYS" - [S->C] Recebe todas as chaves simétricas já por ordem
        
        - Bit commitment:
        "REQUEST_COMMIT" - [S->C] Requer o commit da mão
        "REPLY_COMMIT" - [C->S] Envia o commit
        "REQUEST_REST" - [S->C] Requer o resto do commit
        "REQUEST_REPLY" - [C->S] Envia o resto do commit
        "COMMIT_BROADCAST" - [S->C] Envia todos os bitcommits
        "REST_BROADCAST" - [S->C] Envia o resto
        
        - Base:
        "MAO" - [S->C] Mão inicial para o cliente armazenar !
        "TABULEIRO" - [S->C] Estado atual do tabuleiro 
        "JOGADA" - [C->S] Carta jogada pelo cliente 
        "JOGADA_FINAL" - [S->C] Cartas da ronda finais
        "CHEAT_DETECTED" - [C->S] Foi detectado um jogo incosistente
        "CERT_REQUEST" - [S->C] Requer o certificado do cliente
        "CERT_REPLY" - [C->S] Aceita enviar o certificado
        "CERT_ALL" - [S->C] Partilha todos os certificados
        "DH_REQUEST" - [S->C] Requer a chave publica de diffie hellman do cliente
        "DH_REPLY" - [C->S] Envia a chave publica para o servidor
        "DH_KEYS" - [S->C] Recebe as keys para calcular as chaves partilhadas com todos os clientes
        "OK" - [C->S] Confirmação (quando requerida)
        "DENY" - [C->] Negação, termina o jogo e retira o jogador (quando requerida)
        "FIM" - [S->C] Define o fim de um jogo

        - Assegurar:
        "LISTA DE JOGADORES" - [S->C] Lista dos jogadores que está atualmente na mesa

    - Mensagem:
        "MAO" - Tuplo com cartas
        "TABULEIRO" - Tuplo com cartas
        "JOGADA" - String que define uma carta única
        "OK" - vazio
        "FIM" - Lista com a pontuação atual do jogo
'''

def client_instance(id , host, port):

    print("Temporary Client ID " + str(id) + " started!")
    temp_id = id

    batota = True
    chance_batota = 0.01
    naipes = 'CPOE'
    valores = '23456789TJQKA'
    exemplar = list(''.join(card) for card in itertools.product(naipes, valores))

    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

    s.connect((host,port))

    dh = pyDH.DiffieHellman()
    dh_pk = dh.gen_public_key()
    dh_sk = [0]*5
    dh_f = [0]*5

    # Symetric encription variables
    symkey=os.urandom(16)
    ciph=SymCipher(symkey)


    privK=generate_key()

    if id==1:
        cc,cert=getCCandCert()
        cert=x509.load_pem_x509_certificate(cert,default_backend())
    else:
        cert=generate_self_signed(privK)


    current_hand = []
    # Historico para ter a certeza que não há batota
    played_cards = []
    players_drouth = [[],[],[],[]]

    # Bit commit
    keeping = {}
    commit_all = []
    rest_all = []
    cartas_players = [[],[],[],[]]

    done = False
    while not done:
        data = s.recv(524288)
        mensagem = json.loads(data.decode('ascii'))
        pacote = {}

        if mensagem["tipo"] == "MENU":
            pacote["tipo"] = "SELECT"
            pacote["jogador"] = 0
            #pacote["mensagem"] = input(mensagem["mensagem"])
            pacote["mensagem"] = "1"
        # Enviar o certificado
        if mensagem["tipo"] == "CERT_REQUEST":
            temp_id = mensagem["jogador"]
            pacote["mensagem"] = cert.public_bytes(serialization.Encoding.PEM).decode("utf-8")
            pacote["tipo"] = "CERT_REPLY"
            pacote["jogador"] = temp_id
        # Verificar se quer jogar com aqueles jogadores
        if mensagem["tipo"] == "CERT_ALL":

            if (not verify_certificate_chain(mensagem["mensagem"][0])):
                print("certificate chain is currupted")
                s.close()
                return

            print("Jogador ", temp_id , ": I agree to play with this people")
            pacote["mensagem"] = ""
            pacote["tipo"] = "OK"
            pacote["jogador"] = temp_id
        if mensagem["tipo"] == "DH_REQUEST":
            pacote["mensagem"] = dh_pk
            pacote["tipo"] = "DH_REPLY"
            pacote["jogador"] = temp_id
        if mensagem["tipo"] == "DH_KEYS":
            pacote["mensagem"] = ""
            pacote["tipo"] = "OK"
            pacote["jogador"] = temp_id
            for i in range(4):
                if i == int(temp_id)-1:
                    print("Jogador ", temp_id, ": will skip", i)
                    ti = 4
                else:
                    ti = i
                dh_sk[ti] = dh.gen_shared_key(mensagem["mensagem"][ti])
                dh_f[ti] = generateKeyDH(dh_sk[ti])
            #pacote["mensagem"] = dh_f[4].encrypt("OK".encode()).decode('utf-8')
            print("Jogador ", temp_id, ": ", dh_sk)
            print("Jogador ", temp_id, ": ", dh_f)
        # Dependendo do cabeçalho da mensagem recebido, reagir de forma diferente
        if mensagem["tipo"] == "BARALHO":
            baralho = mensagem["mensagem"]
            random.shuffle(baralho)
            for i in range(len(baralho)):
                try:
                     baralho[i] =  baralho[i].decode()
                except (UnicodeDecodeError, AttributeError):
                    pass
                #Uncoment to correct version
                baralho[i]=ciph.encrypt(baralho[i].encode()).decode()

                #Comment this part
               # baralho[i]=ciph.encrypt(baralho[i].encode())
               # baralho[i]=ciph.decrypt(baralho[i]).decode()
            pacote["mensagem"] = baralho
            pacote["tipo"] = "DEVOLVER"
            pacote["jogador"] = temp_id

        if mensagem["tipo"] == "DISTRIBUICAO": # enviar sym key
            '''
            Steps para distribuir:
                1) Decifrar com a chave Diffie Hellman do jogador source (se vier do servidor não tem diffie-hellman TODO Decidir se fica assim)
                2.1) Se não tem mais cartas válidas, decidir se aviso que acabou ou se mando de volta
                2.2) Decidir se tiro carta ou troco (se tiver cartas)
                3) Escolher um destino aleatório
                4) Cifrar com o destino e trocar source/destination
            '''
            print("Jogador " + str(temp_id) + ": Recebi o deck")
            time.sleep(0.05)
            hipoteses = list(range(1,temp_id)) + list(range(temp_id+1,5))
            if mensagem["jogador"][0] != 0: # Se for sem ser do server, decifrar
                print("Jogador " + str(temp_id) + ": Vou decifrar com: " + str(mensagem["jogador"][0]))
                current_deck = json.loads(dh_f[int(mensagem["jogador"][0])-1].decrypt(mensagem["mensagem"].encode()).decode('utf-8'))
            else:
                current_deck = json.loads(mensagem["mensagem"])
            if current_deck[1] == 0: # Se não tiver carta nenhuma
                if random.random() < 0.9: # Chance de avisar que terminou
                    pacote["tipo"] = "FIM_DISTRIBUICAO"
                    pacote["jogador"] = temp_id
                    pacote["mensagem"] = ""
                else: # Chance de mandar para o próximo
                    pacote["tipo"] = "DISTRIBUICAO"
                    pacote["jogador"] = [temp_id, random.choice(hipoteses)]
                    pacote["mensagem"] = dh_f[int(pacote["jogador"][1])-1].encrypt(json.dumps(current_deck).encode()).decode('utf-8')
            else: # Se tiver cartas ainda
                current_valid = current_deck[1]
                all_cards = current_deck[0]
                if len(current_hand) < 13: # Decidir se tiro 1
                    if random.random() < 0.9:
                        card = all_cards.pop(random.randint(0,current_valid-1))
                        current_hand.append(card)
                        all_cards.append(card)
                        current_valid -= 1
                        print("Jogador " + str(temp_id) + ": Tirei uma carta("+ str(card) +") sobram: " + str(current_valid))
                if len(current_hand) != 0 and current_valid > 0: # Se tiver cartas posso trocar
                    for c in range(len(current_hand)):
                        if random.random() < 0.05: # Chance de querer trocar
                            old_card = current_hand[c]
                            pos = random.randint(0, current_valid-1)
                            new_card = all_cards[pos]
                            current_hand[c] = new_card
                            all_cards[pos] = old_card
                pacote["tipo"] = "DISTRIBUICAO"
                pacote["jogador"] = [temp_id, random.choice(hipoteses)]
                current_deck = json.dumps([all_cards,current_valid])
                print("Jogador " + str(temp_id) + ": Vou cifrar com: " + str(pacote["jogador"][1]))
                try:
                    pacote["mensagem"] = dh_f[int(pacote["jogador"][1])-1].encrypt(current_deck.encode()).decode('utf-8')
                except:
                    print(dh_sk)
                    sys.exit(0)

        if mensagem["tipo"] == "REQUEST_COMMIT": # a receber todas as chaves simétricas
            print("Jogador " + str(temp_id) + ": vou entregar o bit commit")
            keeping, sending = bitCommitment(current_hand)
            #print("Keep: " + str(keeping) + " | Publish: " + str(sending))
            pacote["tipo"] = "REPLY_COMMIT"
            pacote["jogador"] = temp_id
            pacote["mensagem"] = sending

        if mensagem["tipo"] == "REQUEST_REST": # a receber todas as chaves simétricas
            print("Jogador " + str(temp_id) + ": vou decifrar o bit commit")
            pacote["tipo"] = "REPLY_REST"
            pacote["jogador"] = temp_id
            pacote["mensagem"] = keeping

        if mensagem["tipo"] == "COMMIT_BROADCAST":
            print("Jogador " + str(temp_id) + ": recebi os bit commits")
            commit_all = mensagem["mensagem"]
            pacote["tipo"] = "OK"
            pacote["jogador"] = temp_id
            pacote["mensagem"] = ""

        if mensagem["tipo"] == "REST_BROADCAST":
            print("Jogador " + str(temp_id) + ": recebi o resto dos bit commits")
            rest_all = mensagem["mensagem"]
            for i in range(4):
                mao = rest_all[i]["C"].split(",")
                if validateBitCommitment(rest_all[i], commit_all[i]):  # Verdade e decifra a mão
                    for tid in range(3, -1, -1):  # Trocar a ordem
                        aes = SymCipher(base64.b64decode(symKeys[tid].encode('utf-8')))
                        for f in range(len(mao)):
                            mao[f] = (aes.decrypt(mao[f])).decode()
                    print("Jogador " + str(id) + ": tem na mão " + str(mao))
                    # TODO Verificar se são as cartas que ele jogou
                    if len(set(mao) - set(cartas_players[i])) == 0:
                        pacote["tipo"] = "OK"
                        pacote["jogador"] = temp_id
                        pacote["mensagem"] = ""
                    else:
                        print("Jogador " + str(temp_id) + ": Detectei batota do tipo CARD_FORGING pelo jogador " + str(i))
                        pacote["tipo"] = "CHEAT_DETECTED"
                        pacote["jogador"] = temp_id
                        pacote["mensagem"] = "CARD_FORGING"
                else:  # Não é verdade e chama-lhe nomes por ser batoteiro
                    print("Jogador " + str(temp_id) + ": Detectei batota do tipo INVALID_FIRSTHAND pelo jogador " + str(i))
                    pacote["tipo"] = "CHEAT_DETECTED"
                    pacote["jogador"] = temp_id
                    pacote["mensagem"] = "INVALID_FIRSTHAND"


        if mensagem["tipo"] == "SHUFFLE_KEY_REQUEST": # enviar sym key
            print("Jogador " + str(temp_id) + ": a enviar a chave simétrica " + base64.b64encode(symkey).decode('utf-8'))
            pacote["tipo"] = "SHUFFLE_KEY_REPLY"
            pacote["jogador"] = temp_id
            pacote["mensagem"] = base64.b64encode(symkey).decode('utf-8')

        if mensagem["tipo"] == "SHUFFLE_KEYS": # a receber todas as chaves simétricas
            print("Jogador " + str(temp_id) + ": a receber todas as chaves simétricas")
            # Decifrar por ordem as cartas
            symKeys = mensagem["mensagem"]
            for tid in range(3,-1,-1): # Trocar a ordem
                aes = SymCipher(base64.b64decode(symKeys[tid].encode('utf-8')))
                for i in range(len(current_hand)):
                    current_hand[i] = (aes.decrypt(current_hand[i])).decode()
            print("Jogador " + str(temp_id) + ": tem na mão "+ str(current_hand))
            if batota:
                if random.random() < chance_batota:
                    print("Jogador " + str(temp_id) + ": vai fazer batota e trocar uma carta por outra")
                    current_hand[random.randint(0,len(current_hand)-1)] = exemplar[random.randint(0,len(exemplar)-1)]
            pacote["tipo"] = "OK"
            pacote["jogador"] = temp_id
            pacote["mensagem"] = ""

        if mensagem["tipo"] == "MAO": # Receber e armazenar a mão
            print("TOU A RECEBER MAO" + str(mensagem["mensagem"]))
            baralho = mensagem["mensagem"]
            if len(baralho) != 0:
                current_hand.append(baralho.pop(random.randrange(len(baralho))))

            #print(len(mao))
            #print("[Jogador " + str(temp_id) +"] Tirei a seguinte carta: " + str(mao))
            if len(current_hand)==13:
                starting_cards = current_hand
                played_cards = []
                players_drouth = [[],[],[],[]]
            pacote["tipo"] = "BARALHO"
            pacote["jogador"] = temp_id
            pacote["mensagem"] = baralho
        if mensagem["tipo"] == "TABULEIRO": # Receber o tabuleiro atual e decidir a jogada
            # Decidir carta
            # TODO: ADICIONAR CHANCE DE BATOTA
            print("[Jogador " + str(temp_id) +"] Recebi que o tabuleiro está assim: " + str(mensagem["mensagem"]))
            pacote["tipo"] = "JOGADA"
            pacote["jogador"] = temp_id
            #carta = mao.pop(random.randint(0,len(mao)-1))
            # Caso não tenha nenhuma carta, joga a que quiser
            if len(mensagem["mensagem"]) == 0:
                carta = current_hand.pop(random.randint(0, len(current_hand)-1))
            else: # Caso tenha:
                possiveis = []
                for m in current_hand:
                    if mensagem["mensagem"][0][0] == m[0]: # Caso tenha do naipe, joga essa
                        possiveis.append(m)
                if len(possiveis) == 0: # Caso não tenha joga uma qualquer
                    carta = current_hand.pop(random.randint(0,len(current_hand)-1))
                else:
                    carta = possiveis[random.randint(0,len(possiveis)-1)]
                    current_hand.remove(carta)
            print("[Jogador " + str(temp_id) +"] Vou jogar a carta: " + carta)
            if batota:
                if random.random() < chance_batota:
                    print("Jogador " + str(temp_id) + ": vai fazer batota e jogar uma carta que não tem")
                    carta = exemplar[random.randint(0,len(exemplar)-1)]


            # Menssagem passa a ser constituida por carta + assinatura desta + chave publica
            msg={}
            msg["carta"]=carta

            if id==1:
                signature=signCCMsg(msg["carta"])
            else:
                signature=privK.sign(msg["carta"].encode(),
                                            padding.PSS(
                                                mgf=padding.MGF1(hashes.SHA256()),
                                                salt_length=padding.PSS.MAX_LENGTH
                                                ),
                                            hashes.SHA256())
            

            msg["assinatura"]=base64.b64encode(signature).decode('utf-8')
            pemPub = privK.public_key().public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            #print(pemPub)

            msg["pubKey"]=base64.b64encode(pemPub).decode('utf-8')
            pacote["mensagem"] = json.dumps(msg)
            
        if mensagem["tipo"] == "FINAL_JOGADA":
            print("[Jogador " + str(temp_id) +"] Fim da ronda: " + str(mensagem["mensagem"]))
            # Tipos de batota necessário verificar:
            #   - Se foi jogada a mesma carta 2x
            #   - Se foi jogada uma carta que eu tenho
            #   - Se foi renuncia (jogada uma carta de um naipe que uma vez não tinha e agora já tem)
            # Preparation
            cheat = "NONE"
            ronda = mensagem["mensagem"][0]
            naipe = mensagem["mensagem"][1]
            for i in range(4):
                cartas_players[i].append(ronda[i])
            played_cards += ronda
            # Cheat verification
            if len(played_cards) != len(set(played_cards)):
                cheat = "DUPLICATED_CARD"
            if len(set(ronda).intersection(set(current_hand))) != 0:
                cheat = "MY_CARD"
            for f in range(4):
                if ronda[f][0] in players_drouth[f]:
                    print("JOGADOR " + str(f) + " fez renuncia com a carta " + ronda[f])
                    print(players_drouth)
                    cheat = "RENUNCIA"
                if ronda[f][0] != naipe:
                    players_drouth[f] += [naipe]

            # Montar pacote
            if cheat == "NONE":
                pacote["tipo"] = "OK"
                pacote["jogador"] = temp_id
                pacote["mensagem"] = ""
            else:
                print("Jogador "+str(temp_id)+": Detectei batota do tipo " + cheat)
                pacote["tipo"] = "CHEAT_DETECTED"
                pacote["jogador"] = temp_id
                pacote["mensagem"] = cheat
        if mensagem["tipo"] == "FIM_RONDA":
            print("[Jogador " + str(temp_id) +"] terminou a ronda")
            pacote["tipo"] = "OK"
            pacote["jogador"] = temp_id
            pacote["mensagem"] = ""
            if len(current_hand) != 0:
                print("Jogador "+str(temp_id)+": Jogo acabou abruptamente")
            current_hand = []
            # Historico para ter a certeza que não há batota
            played_cards = []
            players_drouth = [[], [], [], []]
            keeping = {}
            commit_all = []
            rest_all = []
            cartas_players = [[],[],[],[]]
        if mensagem["tipo"] == "FIM":
            print("[Jogador " + str(temp_id) +"] Acabei")
            
          
            if json.loads(mensagem["mensagem"])["pontuacao"][temp_id-1] > 100:
                print("Jogador "+str(temp_id) +" perdeu!")
            
            pacote["tipo"] = "OK"
            pacote["jogador"] = temp_id


            if id == 1:
                signature = signCCMsg(mensagem["mensagem"])
            else:
                signature=privK.sign(mensagem["mensagem"].encode(),
                                            padding.PSS(
                                                mgf=padding.MGF1(hashes.SHA256()),
                                                salt_length=padding.PSS.MAX_LENGTH
                                                ),
                                            hashes.SHA256())



            pemPub = privK.public_key().public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            

            #base64.b64encode(pemPub).decode('utf-8')
            pacote["mensagem"] = base64.b64encode(signature).decode('utf-8')
            done = True
        s.send(json.dumps(pacote).encode('ascii'))

    # close the connection
    print("[Jogador " + str(temp_id) + "] Acabei")
    s.close()

def play_bot_game():
    # Parameters
    host = '127.0.0.1'
    port = 12345
    clients_number = 4

    # Collect the threads
    threads = []
    for i in range(clients_number):
        # Create 9 threads counting 10-19, 20-29, ... 90-99.
        thread = threading.Thread(target=client_instance, args=(i + 1, host, port))
        threads.append(thread)

    # Start them all
    for thread in threads:
        thread.start()

    # Wait for all to complete
    for thread in threads:
        thread.join()

def menu():
    print("*** MENU ***")
    print("1. Simular jogo")
    print("2. Ver Histórico")

    print("---------------------")
    print("0. Terminate")
    print("---------------------")
    return int(input("Option: "))


def Main():
    print(".------..------..------..------..------.")
    print("|C.--. ||O.--. ||P.--. ||A.--. ||S.--. |")
    print("| :/\: || :/\: || :/\: || (\/) || :/\: |")
    print("| :\/: || :\/: || (__) || :\/: || :\/: |")
    print("| '--'C|| '--'O|| '--'P|| '--'A|| '--'S|")
    print("`------'`------'`------'`------'`------'")
    print("                                        ")

    host = '127.0.0.1'
    port = 12344
    sm = socket.socket()
    sm.connect(('127.0.0.1', port))

    while (True):
        print("\n" + "\n")
        op = menu()

        if op == 0:
            break
        if op == 1:
            play_bot_game()
        if op == 2:
            sm.send(b"LIST")
            options = sm.recv(512)
            print(json.loads(options.decode('utf-8')))
            receipt_name = input("Choose a receipt to read:")
            sm.send(receipt_name.encode())
            receipt = sm.recv(8192)
            data = json.loads(receipt.decode('utf-8'))

            history = data["history"]
            player = []
            plays = []
            for play in json.loads(history["recibo"]["message"]):
                p = json.loads(play)
                plays.append([p["jogador"], p["jogada"]])
                player.append(str(p["jogador"]))
                plays.append(p["jogada"])
            print(tabulate(plays, headers=['Jogador', 'Jogada'],tablefmt='grid'))
            print("Pontuação: " + str(history["pontuacao"]))



    print("Exiting client program")
    sm.close()

if __name__ == '__main__':
	Main()

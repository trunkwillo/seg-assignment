import random
import itertools
from blockchain import BlockChain

class Copas:

    ronda = 1
    baralho = []
    tabuleiro = []
    
    
    # Possivelmente randomizar a ordem e fazer dinâmico o nº de jogadores mas para já 4 fixos
    turno = [0]
    pontuacao = [0, 0, 0, 0]
    fim = False

    pilha = [[],[],[],[]]
    len_pilha = 0
    ordem = []

    def __init__(self):
        naipes = 'CPOE'
        valores = '23456789TJQKA'
        self.baralho = list(''.join(card) for card in itertools.product(naipes, valores))
        # BlockChain
        self.blockChain=BlockChain()

        self.ronda = 1
        self.tabuleiro = []
        # Possivelmente randomizar a ordem e fazer dinâmico o nº de jogadores mas para já 4 fixos
        self.turno = [0]
        self.pontuacao = [0, 0, 0, 0]
        self.fim = False
        self.batota = False

        self.pilha = [[], [], [], []]
        self.len_pilha = 0
        self.ultima_ordem = []
        self.ultima_ronda = []

        # Histórico
        self.played_cards = []
        self.players_drouth = [[], [], [], []]


    '''
        Inicialização
    '''
    def baralhar(self):
        random.shuffle(self.baralho)
        return


    def receberMao(self):
        temp = self.baralho[-13:]
        del self.baralho[-13:]
        if len(self.baralho) == 0: # Se tiver dado as cartas todas, iniciar o jogo com turnos aleatórios
            self.iniciarTurno(random.randint(1,4))
        return temp


    '''
        Reiniciar (TODO: MUDAR O INIT PARA SER UMA FUNÇÃO À PARTE A CORRER DAQUI KTNXBYE)
    '''
    def reiniciar(self):
        naipes = 'CPOE'
        valores = '23456789TJQKA'
        self.baralho = list(''.join(card) for card in itertools.product(naipes, valores))
        self.fim = False
        self.pilha = [[],[],[],[]]
        self.len_pilha = 0
        self.ronda = 1
        self.iniciarTurno(random.randint(1,4))

        self.played_cards = []
        self.players_drouth = [[], [], [], []]


    '''
        Jogar
    '''
    def jogarCarta(self, id, carta):
        if self.turno[0] != id:
            print("TA MALI, TA ERRADO")
            return False
        self.ordem.append(self.turno.pop(0))
        # Verificar se a carta é válida TODO
        self.tabuleiro.append(carta)
        self.blockChain.createBlock('{"jogador":'+str(id)+', "jogada": "'+carta+'"}')
        if len(self.tabuleiro) == 4: # Escolha do vencedor
            validas = []
            for c in self.tabuleiro:
                if self.tabuleiro[0][0] == c[0]:
                    validas.append(c)
            validas = sorted(validas, key=lambda x: x[1])
            #print(self.tabuleiro.index[validas[0]])
            vencedor = self.ordem[self.tabuleiro.index(validas[0])]
            print("Venceu o jogador:" + str(vencedor))
            self.ultima_ordem = self.ordem
            self.ultima_ronda = self.tabuleiro
            self.pilha[vencedor-1] += self.tabuleiro
            self.len_pilha += 4

            # Verificação de batota
            ronda = self.ultima_ronda
            naipe = self.ultima_ronda[0][0]
            o1 = self.ordem[0]
            ronda = ronda[4 - (o1 - 1):] + ronda[:4 - (o1 - 1)]
            self.played_cards += self.ultima_ronda
            if len(self.played_cards) != len(set(self.played_cards)):
                cheat = "DUPLICATED_CARD"
                self.setBatota(0,cheat)
                return False
            for f in range(4):
                if ronda[f][0] in self.players_drouth[f]:
                    print("JOGADOR " + str(f) + " fez renuncia com a carta " + ronda[f])
                    print(self.players_drouth)
                    cheat = "RENUNCIA"
                if ronda[f][0] != naipe:
                    self.players_drouth[f] += [naipe]

            if self.len_pilha != 52:
                self.ronda += 1
                self.iniciarTurno(vencedor)
            else:
                self.fim = True
                self.calcularPontos()
            return True
        return False


    def verTabuleiro(self):
        return self.tabuleiro

    def verificarTurno(self):
        return self.turno

    def verificarFim(self):
        return self.fim

    def setBatota(self, id, cheat):
        self.fim = True
        self.batota = True
        self.pontuacao[id-1] += 100


    '''
        Auxiliares
    '''
    # Escolhendo o primeiro jogador, define os turnos para a direita
    def iniciarTurno(self,n):
        print("Vai começar a ronda " + str(self.ronda) + "!")
        self.tabuleiro = []
        self.ordem = []
        self.turno = [1, 2, 3, 4]
        self.turno = self.turno[n-1:] + self.turno[:n-1]
        return


    def calcularPontos(self):
        for i in range(len(self.pontuacao)):
            shoot_the_moon = 0
            temp_score = 0
            for c in self.pilha[i]:
                if c[0] == "C":
                    shoot_the_moon += 1
                    temp_score += 1
                elif c == "EQ":
                    shoot_the_moon += 1
                    temp_score += 13
            if shoot_the_moon < 13:
                self.pontuacao[i] += temp_score
            else:
                for f in range(len(self.pontuacao)):
                    if f != i:
                        self.pontuacao[i] += 26
                break
        print(self.pontuacao)
        return

    def over(self):
        if self.batota:
            return True
        for i in self.pontuacao:
            if i >= 5:
                return True
        return False


import json
from tabulate import tabulate
import os
import socket


def loadRecibo(file):
    with open(os.getcwd() + "/Recibos/"+file) as json_file:
        data = json.load(json_file)
        history=data["history"]
        player=[]
        plays=[]
        for play in json.loads(history["recibo"]["message"]):
            p=json.loads(play)
            plays.append([p["jogador"],p["jogada"]])
            #player.append(str(p["jogador"]))
            #plays.append(p["jogada"])
        
        return data
        # print(tabulate(plays, headers=['Jogador', 'Jogada'],tablefmt='grid'))
        # print("Pontuação: " + str(history["pontuacao"]))

def listRecibos():
    files = []
    i = 1
    for file in os.listdir(os.getcwd() + "/Recibos"):
        files.append(file)
        #print(str(i) + ". " + file)
        i = i + 1
    return files


def main():
    host = '127.0.0.1'
    port = 12344
    s = socket.socket()
    s.bind(('', port))
    print("socket binded to %s" % (port))

    s.listen(5)
    print("socket is listening")

    # a forever loop until we interrupt it or
    # an error occurs
    while True:
        # Establish connection with client.
        c, addr = s.accept()
        print('Got connection from', addr)

        option = c.recv(512)
        if option.decode('utf-8') == "LIST":
            c.send(json.dumps(listRecibos()).encode())
            receipt_name = c.recv(512)
            c.send(json.dumps(loadRecibo(receipt_name.decode('utf-8'))).encode())

main()
import socket
from _thread import *
import threading
from table import Copas
import json
import sys
import random

from cryptography.x509.oid import NameOID
import datetime
import base64
from Crypto.Cipher import AES
from Crypto import Random
import pyDH
from cryptography import x509
from cryptography.hazmat.primitives import serialization
from Crypto.Hash import SHA256
import base64
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives.asymmetric import rsa, padding

from cryptography.fernet import Fernet

import time
from cryptography.hazmat.primitives.serialization import load_pem_public_key

import os

import OpenSSL
from OpenSSL import crypto


#import sys
#sys.path.insert(0, '../securityUtils/')
#from .securityUtils import securityUtils



'''
    Protocolo de comunicação:
    - Jogador (Nota: 0 é o coupier)
    - Tipo de mensagem:
        - Base:
        "MAO" - [S->C] Mão inicial para o cliente armazenar
        "TABULEIRO" - [S->C] Estado atual do tabuleiro
        "JOGADA" - [C->S] Carta jogada pelo cliente
        "OK" - [C->S] Confirmação (quando requerida)
        "FIM" - [S->C] Define o fim de uma ronda

        - Assegurar:
        "LISTA DE JOGADORES" - [S->C] Lista dos jogadores que está atualmente na mesa

    - Mensagem:
        "MAO" - Tuplo com cartas
        "TABULEIRO" - Tuplo com cartas
        "JOGADA" - String que define uma carta única
        "OK" - vazio
        "FIM" - Lista com a pontuação atual do jogo
'''


class SymCipher:
    def __init__( self, key ):
        self.key = key
        #add X '\0' till size is AES block
        self.pad = lambda s: s + b"\0" * ((AES.block_size - len(s)) % AES.block_size)
        #remove '\0''s
        self.unpad = lambda s: s.rstrip(b"\0")

    def encrypt( self, data ):
        data = self.pad(data)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new( self.key, AES.MODE_CBC, iv )

        return base64.b64encode(iv+cipher.encrypt(data))

    def decrypt( self, enc_data ):
        enc_data = base64.b64decode(enc_data)
        iv=enc_data[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return self.unpad(cipher.decrypt( enc_data[AES.block_size:] ))


def verify_certificate_chain(certificate):
    trusted_certs = []
    crl_list = []
    print(os.getcwd()+"/Certs")
    for files in os.listdir(
            os.getcwd()+"/Certs"):
        if files == ".DS_Store":
            continue


        _cert = open(
            os.getcwd()+"/Certs/" + files,
            'rb').read()
        trusted_certs.append(_cert)

    for files in os.listdir(
            os.getcwd() + "/CRL"):
        if files == ".DS_Store":
            continue


        _crl = open(
            os.getcwd() + "/CRL/" + files,
            'rb').read()
        crl_list.append(_crl)

    # cc,certificate=getCCandCert()
    certificate = crypto.load_certificate(crypto.FILETYPE_PEM, certificate)
    store = crypto.X509Store()

    for _cert in trusted_certs:
        try:
            pemCert = crypto.load_certificate(crypto.FILETYPE_ASN1, _cert)
            store.add_cert(pemCert)

        except:
            try:
                pemCert = crypto.load_certificate(crypto.FILETYPE_PEM, _cert)
                store.add_cert(pemCert)

            except Exception as e:
                continue

    for _crl in crl_list:
        try:
            pcrl = crypto.load_crl(crypto.FILETYPE_ASN1, _crl)
            store.add_crl(pcrl)

        except:
            try:
                pcrl = crypto.load_crl(crypto.FILETYPE_PEM, _crl)
                store.add_crl(pcrl)

            except Exception as e:
                continue

    store_ctx = crypto.X509StoreContext(store, certificate)

    try:
        store_ctx.verify_certificate()
        return True


    except Exception as e:
        print(e)
        return False


def generate_self_signed(key):
    subject = issuer = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"PT"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Aveiro"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Aveiro"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"UA"),
        x509.NameAttribute(NameOID.COMMON_NAME, u"cert.ua.pt"),
    ])
    cert = x509.CertificateBuilder().subject_name(
        subject
    ).issuer_name(
        issuer
    ).public_key(
        key.public_key()
    ).serial_number(
     x509.random_serial_number()
    ).not_valid_before(
        datetime.datetime.utcnow()
    ).not_valid_after(
        datetime.datetime.utcnow() + datetime.timedelta(days=1)
    ).add_extension(
        x509.SubjectAlternativeName([x509.DNSName(u"localhost")]),
        critical=False,
    ).sign(key, hashes.SHA256(), default_backend())

    return cert


#gen priv key
def generate_key():
    key = rsa.generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )
    return key

#Bit Commitment Validation
def validateBitCommitment(kept, published):
    h=SHA256.new()
    toHash=kept["C"]+published["R1"]+kept["R2"]
    h.update(toHash.encode())
    b=h.hexdigest()
    return published["b"]==b


def validateCCSignature(signature,cert, msg):
    try:
        if OpenSSL.crypto.verify(cert,signature,msg.encode('utf-8'),'sha1') == None:
            return True
        return False
    except:
        return False


def generateKeyDH(sw):
    password_provided = sw  # This is input in the form of a string
    password = password_provided.encode()  # Convert to type bytes
    salt = b'ola'  # CHANGE THIS - recommend using a key from os.urandom(16), must be of type bytes
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
        backend=default_backend()
    )
    key = base64.urlsafe_b64encode(kdf.derive(password))  # Can only use kdf once
    f = Fernet(key)
    return f


# Global variables
thread_lock = threading.Lock()
sync_players = threading.Barrier(4)

count = [0]*4
order = []


certs = [0]*4

privK=generate_key()
servCert= generate_self_signed(privK)

dh = pyDH.DiffieHellman()
dh_pk = dh.gen_public_key()

#Diffie-Hellman key list
dh_keys=[0]*4
dh_f = [0]*4

# Baralhar
ordem_sym=[]
symKeys=[0]*4

# Distribuição
done = False
current_source = 0
current_destination = random.randint(1,4)
current_deck = []

#recibo
recibo={}

# Bit commitment
bc_list = [0]*4
hand_list = [0]*4
cartas_players = [[],[],[],[]]


# thread fuction
def threaded(s, table, id):
    global count
    global order
    global certs
    global dh
    global dh_pk
    global dh_keys
    global dh_f
    global done
    global current_source
    global current_destination
    global  current_deck
    global ordem_sym
    global symKeys
    global bc_list
    global hand_list
    global cartas_players

    if id == 1:
        count = [0] * 4
        order = []
        certs = [0] * 4

        # Diffie-Hellman key list
        dh_keys = [0] * 4
        dh_f = [0] * 4

        # Baralhar
        ordem_sym = []
        symKeys = [0] * 4

        # Distribuição
        done = False
        current_source = 0
        current_destination = random.randint(1, 4)
        current_deck = []

        # Bit commitment
        bc_list = [0] * 4
        hand_list = [0] * 4
        cartas_players = [[], [], [], []]



    # Sempre que entra um jogador envia os certificados que já tem
    thread_lock.acquire()
    data = interact(s, id, "CERT_REQUEST", "")
    if data["tipo"] == "CERT_REPLY":
        certs[data["jogador"] - 1] = data["mensagem"]
        if data["jogador"] == 1:

            if(not verify_certificate_chain(data["mensagem"])):
                print("certificate chain is currupted")
                s.close()
                return

    thread_lock.release()

    sync_players.wait()

    # Assim que estão os 4 jogadores, envia para todos os 4 certificados
    thread_lock.acquire()
    data = interact(s, id, "CERT_ALL", certs)
    if data["tipo"] == "OK":
        print("Player " + str(id) + " ready!")

    thread_lock.release()
    # Todos os jogadores estão prontos
    sync_players.wait()

    # Partilhar todas as DH
    thread_lock.acquire()
    data = interact(s, id, "DH_REQUEST", "")
    if data["tipo"] == "DH_REPLY":
        dh_keys[data["jogador"] - 1] = data["mensagem"]
        dh_sk = dh.gen_shared_key(dh_keys[data["jogador"] - 1])
        dh_f[data["jogador"] - 1] = generateKeyDH(dh_sk)
    thread_lock.release()

    sync_players.wait()
    thread_lock.acquire()
    temp_dh = dh_keys + [dh_pk]
    #print("Jogador ", id, " Secret Key: ", dh_f[id-1].encrypt("message".encode()))
    data = interact(s, id, "DH_KEYS", temp_dh)
    #print("Player " + str(id) + " enviou " + dh_f[id-1].decrypt(data["mensagem"].encode()).decode('utf-8'))
    thread_lock.release()



    while True:
        sync_players.wait()
        # Baralhar
        '''
        Steps para baralhar:
            1) Iniciar deck, cifrando TODO
            2) Enviar para todos os jogadores para baralharem e cifrarem
            3) Receber de volta
        '''
        thread_lock.acquire()
        # Baralhar e obter ordem de chegada
        data = interact(s, id, "BARALHO", table.baralho)
        table.baralho = data["mensagem"]
        #print(table.baralho)
        ordem_sym.append(data["jogador"])
        thread_lock.release()
        print("Jogador " + str(id) + " terminou de baralhar")

        sync_players.wait() # Sincronizar todos

        '''
        Steps para distribuir o baralho:
            1) Enviar para um jogador
            2) Re-enviar para o ID definido na mensagem
            3) Esperar pela mensagem fim
        '''

        # Enviar para o primeiro
        # MIGHTDO: Possivelmente cifrar com o Diffie-Hellman do server
        if id == 1:
            current_deck = [table.baralho, 52]

        sync_players.wait()

        while True:
            thread_lock.acquire()
            if done:
                thread_lock.release()
                break
            if current_destination == id:
                # Enviar para ele
                temp = json.dumps(current_deck)
                data = interact(s, [current_source, current_destination], "DISTRIBUICAO", temp)
                time.sleep(0.05)
                if data["tipo"] == "FIM_DISTRIBUICAO":
                    print("Jogador " + str(data["jogador"]) + " enviou que a distribuição terminou")
                    done = True
                    thread_lock.release()
                    break
                current_source = data["jogador"][0]
                current_destination = data["jogador"][1]
                current_deck = data["mensagem"]
                #print(current_deck)
            thread_lock.release()

        sync_players.wait()  # Sincronizar todos

        '''
        Steps para receber commit:
            1) Requesitar o bit commitment das mãos dos jogadores 
            2) Armazenar e enviar em broadcast
            3) Juntar no fim e decifrar a mão original com reverse(symKeys)
        '''
        data = interact(s,id, "REQUEST_COMMIT", "")
        bc_list[data["jogador"]-1] = data["mensagem"]
        sync_players.wait() # Sincronizar todos
        data = interact(s, id, "COMMIT_BROADCAST", bc_list)
        sync_players.wait()

        '''
        Steps para devolver:
            1) Requesitar as chaves simétricas
            2) Ordená-las de acordo com o inverso do armazenado
            2) Enviar para todos TODO
        '''
        thread_lock.acquire()
        data = interact(s,id, "SHUFFLE_KEY_REQUEST", "")
        if id == 1:
            print(ordem_sym)
        symKeys[ordem_sym.index(id)] = data["mensagem"]

        thread_lock.release()
        sync_players.wait()
        data = interact(s,id,"SHUFFLE_KEYS",symKeys)

        sync_players.wait()

        if id == 1:
            table.iniciarTurno(random.randint(1,4))
            print("First goer is: " , str(table.verificarTurno()[0]))

        sync_players.wait()

        while True:
            #sync_players.wait()
            thread_lock.acquire()
            if table.verificarFim():
                #print("Jogador com ID:" + str(id) + " vai reiniciar")
                data = interact(s, 0, "RONDA", "")
                thread_lock.release()
                break
            if len(table.verificarTurno()) != 0:
                if table.verificarTurno()[0] == id:
                    #print("Thread " + str(id) + ": É a minha vez.")
                    # Enviar estado atual do tabuleiro
                    data = interact(s, 0, "TABULEIRO", table.verTabuleiro())

                    if data["tipo"] != "JOGADA":
                        print("LOGIC ERROR")
                        sys.exit(1)

                    # Jogar Carta mas primeiro validar a assinatura
                    pemPub=base64.b64decode(json.loads(data["mensagem"])["pubKey"].encode('utf-8'))

                    #print(pemPub)
                    pubKey=load_pem_public_key(pemPub, backend=default_backend())
                
                    signature=base64.b64decode(json.loads(data["mensagem"])["assinatura"].encode('utf-8'))

                    #print(signature)
                    #print(json.loads(data["mensagem"])["carta"])


                    if data["jogador"]==1:
                        validateCCSignature(signature, certs[0], json.loads(data["mensagem"])["carta"])
                    else:
                        pubKey.verify(
                            signature,
                            json.loads(data["mensagem"])["carta"].encode(),
                            padding.PSS(
                                mgf=padding.MGF1(hashes.SHA256()),
                                salt_length=padding.PSS.MAX_LENGTH
                            ),
                            hashes.SHA256()
                        )

                    table.jogarCarta(data["jogador"], json.loads(data["mensagem"])["carta"])
                    cartas_players[data["jogador"]-1].append(json.loads(data["mensagem"])["carta"])

                    thread_lock.release()
                    # Verificar o fim
                    sync_players.wait()
                    thread_lock.acquire()
                    # Enviar por ordem dos jogadores
                    ur = table.ultima_ronda
                    naipe = ur[0][0]
                    o1 = table.ultima_ordem[0]
                    ur = ur[4-(o1-1):] + ur[:4-(o1-1)]
                    data = interact(s, 0, "FINAL_JOGADA", [ur, naipe])
                    if data["tipo"] != "OK":
                        print("Player " + str(data["jogador"]) + " detectou batota do tipo " + str(data["mensagem"]))
                        # Verificar se mão bate certo com o inicial
                        data = interact(s, id, "REQUEST_REST", "")
                        hand_list[data["jogador"] - 1] = data["mensagem"]
                        mao = hand_list[data["jogador"] - 1]["C"].split(",")
                        if validateBitCommitment(hand_list[data["jogador"] - 1],
                                                 bc_list[data["jogador"] - 1]):  # Verdade e decifra a mão
                            for tid in range(3, -1, -1):  # Trocar a ordem
                                aes = SymCipher(base64.b64decode(symKeys[tid].encode('utf-8')))
                                for i in range(len(mao)):
                                    mao[i] = (aes.decrypt(mao[i])).decode()
                            print("Jogador " + str(id) + ": tem na mão " + str(mao))
                            print("Jogador " + str(id) + ": fds " + str(cartas_players[id-1]))
                            if len(set(mao) - set(cartas_players[id-1])) != 0:
                                print("Jogador " + str(id) + " played cards he didn't had")
                                table.setBatota(data["jogador"], "CARD_FORGING")
                        else:  # Não é verdade e chama-lhe nomes por ser batoteiro
                            print("Jogador " + str(id) + " played with the wrong starting hand")
                            table.setBatota(data["jogador"], "INVALID_FIRSTHAND")
                        #table.setBatota(data["jogador"],data["mensagem"])
                    thread_lock.release()
                else:
                    thread_lock.release()

            #sync_players.wait()

        sync_players.wait()

        # Verificar o fim real (jogador ultrapassa os 100 pontos)
        if table.over():
        
            print("Terminou o jogo com a pontuação: " + str(table.pontuacao))

            pacote={}
            exBC=table.blockChain.exportBlockChain()

            
            pacote["pontuacao"]=table.pontuacao
            signature=privK.sign(exBC.encode(),
                                        padding.PSS(
                                            mgf=padding.MGF1(hashes.SHA256()),
                                            salt_length=padding.PSS.MAX_LENGTH
                                            ),
                                        hashes.SHA256())


            pacote["recibo"]={
                "message": exBC,
                "assinatura": base64.b64encode(signature).decode('utf-8')
            }

           

            data = interact(s, id, "FIM", json.dumps(pacote))

            clientCert=x509.load_pem_x509_certificate((certs[int(data["jogador"])-1]).encode('utf-8'), default_backend())
          
            pubKey=clientCert.public_key().public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            signature=base64.b64decode(data["mensagem"].encode('utf-8'))
            


            if data["jogador"] == 1:
                validateCCSignature(signature, certs[0], json.dumps(pacote))
            else:
                clientCert.public_key().verify(
                    signature,
                    json.dumps(pacote).encode(),
                    padding.PSS(
                        mgf=padding.MGF1(hashes.SHA256()),
                        salt_length=padding.PSS.MAX_LENGTH
                    ),
                    hashes.SHA256()
                )




            recibo["history"]=pacote
            key="assinatura"+str(data["jogador"])
           
            recibo[key]=base64.b64encode(signature).decode('utf-8')
            
            with open(str(datetime.datetime.now().strftime('%S-%M-%H-%m-%d-%Y'))+'recibo.txt', 'w+') as outfile:
                json.dump(recibo, outfile)
            
            break

        sync_players.wait()

        # Verificar se mão bate certo com o inicial
        data = interact(s, id, "REQUEST_REST", "")
        hand_list[data["jogador"] - 1] = data["mensagem"]
        mao = hand_list[data["jogador"] - 1]["C"].split(",")
        if validateBitCommitment(hand_list[data["jogador"] - 1], bc_list[data["jogador"] - 1]):  # Verdade e decifra a mão
            for tid in range(3, -1, -1):  # Trocar a ordem
                aes = SymCipher(base64.b64decode(symKeys[tid].encode('utf-8')))
                for i in range(len(mao)):
                    mao[i] = (aes.decrypt(mao[i])).decode()
            print("Jogador " + str(id) + ": tem na mão " + str(mao))
            if len(set(mao) - set(cartas_players[id-1])) != 0:
                print("Jogador " + str(id) + " played cards he didn't had")
                print("Jogador " + str(id) + ": fds " + str(cartas_players[id-1]))
                table.setBatota(data["jogador"], "CARD_FORGING")
        else:  # Não é verdade e chama-lhe nomes por ser batoteiro
            print("Jogador " + str(id) + " played with the wrong starting hand")
            table.setBatota(data["jogador"], "INVALID_FIRSTHAND")
        sync_players.wait()
        data = interact(s, id, "REST_BROADCAST", hand_list)

        sync_players.wait()

        data = interact(s, id, "FIM_RONDA", table.pontuacao)

        sync_players.wait()

        # Reiniciar mesas
        if id == 1:
            print("A reiniciar a mesa")
            table.reiniciar()
            order = []

            # Baralhar
            ordem_sym = []
            symKeys = [0] * 4

            # Distribuição
            done = False
            current_source = 0
            current_destination = random.randint(1, 4)
            current_deck = []

            # Historico
            cartas_players = [[],[],[],[]]
    s.close()

def interact(s, jogador, tipo, mensagem):
    a = {}
    a["jogador"] = jogador
    a["tipo"] = tipo
    a["mensagem"] = mensagem
    s.send(json.dumps(a).encode('ascii'))
    data = s.recv(524288)
    return json.loads(data.decode('ascii'))

def main():
    # Extracted 99% from https://www.geeksforgeeks.org/socket-programming-multi-threading-python/
    host = ""
    # reverse a port on your computer
    # in our case it is 12345 but it
    # can be anything
    port = 12345
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    print("socket binded to port", port)
    while True:
        print("Getting new game ready!")

        table_ref = Copas()
        # Baralhar
        table_ref.baralhar()
        count = 0

        # put the socket into listening mode
        s.listen(5)
        print("Server is listening")

        # Collect the threads
        threads = []

        # a forever loop until client wants to exit
        while len(threads) != 4:
            # establish connection with client
            c, addr = s.accept()
            print('Connected to :', addr[0], ':', addr[1])
            # Start a new thread and return its identifier
            thread = threading.Thread(target=threaded,args=(c,table_ref, len(threads)+1))
            threads.append(thread)
            threads[-1].start()


        # Wait for all to complete
        for thread in threads:
            thread.join()
        print("Finished game!")
    print("Exiting the Server program")
    s.close()

if __name__ == '__main__':
    main()

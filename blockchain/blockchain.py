import hashlib

class Block:
    #Bloco da Block chain:
    #Tem os seguintes constituintes:
    #id=serve apenas para identificar o bloco
    #play= content da nova jogada a ser adiconada a blockchain
    #prev_hash: hash result do ultimo elemento do Bloco
    #my_block_hash: esta vai ser o resultado do digest da combinacao dos valores anteriores + uma variavel incrementavel Nounce
    #               (que vai servir o proposito de permitir calcular uma nova hash que cumpra o nivel de complexidade requerido/ cryptopuzzle)
    #Nounce: Variavel que incrementa até o resultado da hash ser aceitavel pelo nivel de complexidade (cryptopuzzle)

    def __init__(self, id, play, prev_hash):
        self.id=id
        self.play=play
        self.prev_hash=prev_hash
        #self.nounce=nounce
        self.my_block_hash=self.compute_hash()


    #
    def compute_hash(self):

        shaFunc=hashlib.sha256()
        shaFunc.update((str(self.id)+str(self.play)+str(self.prev_hash)).encode('utf-8'))
        hashRes= shaFunc.hexdigest()

        return hashRes



class BlockChain:

    def __init__(self):
        self.chain=[Block(0, '{"card": B, "sig": 0}', "0")]

    def createBlock(self,play):
        lastBlock=self.chain[-1]
        newBlock=Block(lastBlock.id+1,play,lastBlock.my_block_hash )
        self.chain.append(newBlock)
        
    def isCurrupted(self):
        corrupted=False
        actualValue=self.chain[-1].my_block_hash
        print(actualValue)
        for i in range(1,len(self.chain)):
            value=hashlib.sha256((str(self.chain[i].id)+str(self.chain[i].play)+str(self.chain[i].prev_hash)).encode('utf-8')).hexdigest()
            print(value)
            if(value != self.chain[i].my_block_hash):
                corrupted=True
                break
        return corrupted

"""     def solveCryptoPuzzle(complexity,prev_hash,id,play):
        complexity=int(complexity)
        while(1):
            shaFunc=hashlib.sha256()
            shaFunc.update((str(id)+str(play)+str(prev_hash)+str(nounce)).encode('utf-8'))
            hashRes= shaFunc.hexdigest()
            if(hashRes[0:complexity]=="0"*complexity):
                break
            nounce+=1
        return nounce    """



def Main():
    BC=BlockChain()
    BC.createBlock('{"card": B, "sig": 0}')

    print(BC.chain)
    for i in BC.chain:
        print(i.play)

    BC.chain[1].play="false play"

    for i in BC.chain:
        print(i.play)



    print(BC.isCurrupted())


if __name__ == '__main__':
	Main()
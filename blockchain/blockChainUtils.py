def solveCryptoPuzzle(complexity,prev_hash,id,bid):
    nounce=0
    complexity=int(complexity)
    while(1):
        shaFunc=hashlib.sha256();
        shaFunc.update((str(id)+str(bid)+str(prev_hash)+str(nounce)).encode('utf-8'))
        hashRes= shaFunc.hexdigest()
        if(hashRes[0:complexity]=="0"*complexity):
            break
        nounce+=1
    return nounce
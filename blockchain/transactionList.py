from blockchain import *

class Auction:

    def __init__(self, nome, cc, typeOfAuction, tempMax, nBidMax, description, status, Auction_ID, dynamic ,HiddenID=0):
        self.Auction_ID=Auction_ID
        self.nome = nome
        self.cc = cc
        self.typeOfAuction = typeOfAuction
        self.tempMax = tempMax
        self.nBidMax = nBidMax
        self.description = description
        self.status = status
        self.HiddenID=HiddenID
        self.dynamic=dynamic
        self.blocks = [Block(0, '{"Value": "0","Identity":"0000" }', "0",0)]

    def getBlocks(self):
        return self.blocks

    def __str__(self):
     return str(self.nome) + "," + str(self.cc) + "," + str(self.typeOfAuction) + "," + str(self.tempMax) + "," + str(self.nBidMax) + "," + str(self.description)  + "," + str(self.status)

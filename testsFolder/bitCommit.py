from Crypto.Hash import SHA256
import os
import base64

def bitCommitment(hand):
    handStr=','.join(hand)
    random1= base64.b64encode(os.urandom(16)).decode('utf-8')
    random2= base64.b64encode(os.urandom(16)).decode('utf-8')
    h=SHA256.new()
    toHash=handStr+random1+random2
    h.update(toHash.encode())
    b=h.hexdigest()
    toPublish={}
    toPublish["b"]=b
    toPublish["R1"]=random1
    toKeep={}
    toKeep["R2"]=random2
    toKeep["C"]=handStr

    return [toKeep, toPublish]


def validateBitCommitment(kept, published):
    h=SHA256.new()
    toHash=kept["C"]+published["R1"]+kept["R2"]
    h.update(toHash.encode())
    b=h.hexdigest()
    return published["b"]==b


def Main():
    hand=['KH','QH','JH']

    bc= bitCommitment(hand)
    toKeep=bc[0]
    toPub=bc[1]

    print(validateBitCommitment(toKeep,toPub))
    

if __name__ == '__main__':
	Main()



'''
hand=['KH','QH','JH']
handStr=','.join(hand)

random1= base64.b64encode(os.urandom(16)).decode('utf-8')
random2= base64.b64encode(os.urandom(16)).decode('utf-8')
h=SHA256.new()

toHash=handStr+random1+random2
h.update(toHash.encode())
b=h.hexdigest()
print(b)

toPublish={}
toPublish["b"]=b
toPublish["R1"]=random1
toKeep={}
toKeep["R2"]=random2
toKeep["C"]=handStr
'''




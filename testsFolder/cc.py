#$from PyKCS11 import *
import pkcs11
from pkcs11 import Attribute, ObjectClass, Mechanism
import OpenSSL
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.serialization import load_der_public_key
from cryptography.hazmat.primitives.asymmetric import (padding, rsa, utils)

import os 



def signMsgCC(msg):
        lib = 'C:\Windows\System32\libpteidpkcs11.so'
        lib = pkcs11.lib(lib)
        token = lib.get_token()
        with token.open() as session:
            privateKeys = session.get_objects({Attribute.CLASS: ObjectClass.PRIVATE_KEY})
            privateKey = None
            for key in privateKeys:
                if 'CITIZEN AUTHENTICATION KEY' in str(key):
                    privateKey = key
                    break
            if privateKey:
                signature = privateKey.sign(msg.encode('utf-8'),mechanism = Mechanism.SHA1_RSA_PKCS)
                return signature
            return None


def getCCandCert():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print(dir_path)
    #lib = 'C:\Windows\System32\libpteidpkcs11.so'
    lib = "/mnt/c/Windows/System32/pteidpkcs11.dll"
    lib = pkcs11.lib(lib)
    token = lib.get_token()
    with token.open() as session:
        for cert in session.get_objects({Attribute.CLASS: ObjectClass.CERTIFICATE,}):
            cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_ASN1,cert[Attribute.VALUE],)
            subject = cert.get_subject()
            #Convert to PEM format
            cert = OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM,cert)

            return (subject.serialNumber,cert)


def validateSignature(signature,cert, msg):
        try:
            if OpenSSL.crypto.verify(cert,signature,msg.encode('utf-8'),'sha1') == None:
                return True
            return False
        except:
            return False



def Main():

    cc, cert=getCCandCert()
    

if __name__ == '__main__':
	Main()

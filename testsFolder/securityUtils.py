from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
import datetime

	
import os

'''
    Global Variables
'''
def generate_key():
    key = rsa.generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )
    return key


def print_key(private_key):
    pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.NoEncryption()
    )
    print(pem)


def generate_self_signed(key, common_name):
    subject = issuer = x509.Name([
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"PT"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Aveiro"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Aveiro"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"UA"),
        x509.NameAttribute(NameOID.COMMON_NAME, common_name.encode("utf8")),
    ])
    cert = x509.CertificateBuilder().subject_name(
        subject
    ).issuer_name(
        issuer
    ).public_key(
        key.public_key()
    ).serial_number(
     x509.random_serial_number()
    ).not_valid_before(
        datetime.datetime.utcnow()
    ).not_valid_after(
        datetime.datetime.utcnow() + datetime.timedelta(days=1)
    ).add_extension(
        x509.SubjectAlternativeName([x509.DNSName(u"localhost")]),
        critical=False,
    ).sign(key, hashes.SHA256(), default_backend())

    return cert


def store_key(key, name, passphrase):
     with open("securityUtils/keys/"+name+"key.pem", "wb+") as f:
        f.write(key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.BestAvailableEncryption(str.encode(passphrase)),
        ))


def store_cert(cert, name):
     with open("securityUtils/certs/certificate.pem", "wb+") as f:
        f.write(cert.public_bytes(serialization.Encoding.PEM))



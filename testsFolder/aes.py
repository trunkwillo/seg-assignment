import base64
from Crypto.Cipher import AES
from Crypto import Random
import os
from base64 import b64encode


pad = lambda s: s + b"\0" * ((AES.block_size - len(s)) % AES.block_size)
unpad = lambda s: s.rstrip(b"\0") 




class SymCipher:
    def __init__( self, key ):
        self.key = key
      #  self.iv = Random.new().read(AES.block_size) 

    def encrypt( self, data ):
        data = pad(data)
        iv = Random.new().read(AES.block_size) 
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
          
        return base64.b64encode(iv+cipher.encrypt(data)) 

    def decrypt( self, enc_data ):   
        enc_data = base64.b64decode(enc_data)
        iv=enc_data[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc_data[AES.block_size:] ))


def Main():
    key=os.urandom(16)
    print(key)
    print(base64.b64encode(key).decode('utf-8'))
    print(base64.b64decode(base64.b64encode(key)))
   
    aes=SymCipher(key)
    cip=aes.encrypt(("2"*16).encode())
    #print(cip)
    cip=aes.encrypt(cip)
    #print(cip)
    #print(aes.decrypt(aes.decrypt(cip)))


if __name__ == '__main__':
	Main()







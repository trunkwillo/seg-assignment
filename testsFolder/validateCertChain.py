import os

import pkcs11
from pkcs11 import Attribute, ObjectClass, Mechanism
import OpenSSL
from OpenSSL import crypto

def getCCandCert():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print(dir_path)

    lib = "/usr/local/lib/libpteidpkcs11.dylib"
    lib = pkcs11.lib(lib)
    token = lib.get_token()
    with token.open() as session:
        for cert in session.get_objects({Attribute.CLASS: ObjectClass.CERTIFICATE,}):
            cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_ASN1,cert[Attribute.VALUE],)
            subject = cert.get_subject()

            cert = OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM,cert)

            return (subject.serialNumber,cert)



def load_certChain():
    trusted_certs=[]
    crl_list=[]
    print(os.getcwd())
    for files in os.listdir("/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/Certs"):
        if files== ".DS_Store":
            continue

        print(files)
        _cert=open("/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/Certs/"+files,'rb').read()
        trusted_certs.append(_cert)

    for files in os.listdir(
            "/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/CRL"):
        if files == ".DS_Store":
            continue

        print(files)
        _crl = open(
            "/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/CRL/" + files,
            'rb').read()
        crl_list.append(_crl)

    cc,cert=getCCandCert()
    certificate = crypto.load_certificate(crypto.FILETYPE_PEM, cert)
    print(verify_certificate_chain(certificate,trusted_certs,crl_list))

def verify_certificate_chain(certificate):
    trusted_certs = []
    crl_list = []
    print(os.getcwd())
    for files in os.listdir(
            "/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/Certs"):
        if files == ".DS_Store":
            continue

        print(files)
        _cert = open(
            "/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/Certs/" + files,
            'rb').read()
        trusted_certs.append(_cert)

    for files in os.listdir(
            "/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/CRL"):
        if files == ".DS_Store":
            continue

        print(files)
        _crl = open(
            "/Users/diniscanastro/Documentos/Escola/A4S1/Segurança/Projecto-Melhoria/seg-assignment/testsFolder/CRL/" + files,
            'rb').read()
        crl_list.append(_crl)

    #cc,certificate=getCCandCert()
    certificate = crypto.load_certificate(crypto.FILETYPE_PEM, certificate)
    store = crypto.X509Store()

    for _cert in trusted_certs:
        try:
            pemCert=crypto.load_certificate(crypto.FILETYPE_ASN1, _cert)
            store.add_cert(pemCert)

        except:
            try:
                pemCert=crypto.load_certificate(crypto.FILETYPE_PEM, _cert)
                store.add_cert(pemCert)

            except Exception as e:
                continue

    for _crl in crl_list:
        try:
            pcrl=crypto.load_crl(crypto.FILETYPE_ASN1, _crl)
            store.add_crl(pcrl)

        except:
            try:
                pcrl=crypto.load_crl(crypto.FILETYPE_PEM, _crl)
                store.add_crl(pcrl)

            except Exception as e:
                continue
                

    store_ctx = crypto.X509StoreContext(store, certificate)

    try:
        store_ctx.verify_certificate()
        return True


    except Exception as e:
        print(e)
        return False


def main():
    print(verify_certificate_chain())


main()
